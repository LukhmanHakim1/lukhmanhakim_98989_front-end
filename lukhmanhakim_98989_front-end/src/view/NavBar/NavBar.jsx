import React, { useState, useEffect } from 'react';
import { NavLink, useLocation } from 'react-router-dom'; // Import NavLink and useLocation
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faSignOut } from '@fortawesome/free-solid-svg-icons';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import LoginModal from '../../../src/view/Modal/ModalLogin';
import "../../../src/app/navBar.css";

function NavBar() {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [username, setUsername] = useState('');
    const storedUsername = sessionStorage.getItem('username');
    const location = useLocation(); 

    useEffect(() => {
        
        const storedUserData = sessionStorage.getItem('userData');
    
            if (storedUserData) {
            
                const userData = JSON.parse(storedUserData);

                const username = userData.username || '';

                setUsername(username);
            }
            
    }, []); 

    const handleShowModal = () => {
        setIsModalVisible(true);
    };

    const handleLogout = () => {
        sessionStorage.clear();
        setUsername("");
        window.location.href = 'http://localhost:3000/home'
    };

    return (
        <>
        <div className='mobile-navbar'>
        <Navbar expand="lg" bg="dark" variant="dark" style={{ width: 'auto', height: '104px' }}>
                <Container>
                    <Navbar.Brand href="/home" style={{ fontWeight: 'bold' }}>
                        PcariMovie
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link as={NavLink} to='/home' exact="true" className={`nav-link-hover ${location.pathname === '/home' && 'active'}`}>Home</Nav.Link>
                            {username ? (
                                <>
                                    <Nav.Link as={NavLink} to="/searchMovie" className={`nav-link-hover ${location.pathname === '/searchMovie' && 'active'}`}>Search Movie</Nav.Link>
                                    <Nav.Link as={NavLink} to="/searchMovieDate" className={`nav-link-hover ${location.pathname === '/searchMovieDate' && 'active'}`}>Search Movie By Date</Nav.Link>
                                </>
                            ) : (
                               null
                            )}
                            <Nav.Link as={NavLink} to="/ticketPrice" className={`nav-link-hover ${location.pathname === '/ticketPrice' && 'active'}`}>Pricing</Nav.Link>
                            <Nav.Link as={NavLink} to="/movie" className={`nav-link-hover ${location.pathname === '/movie' && 'active'}`}>Movie</Nav.Link>
                            <Nav.Link as={NavLink} to="/tvShow" className={`nav-link-hover ${location.pathname === '/tvShow' && 'active'}`}>TV Show</Nav.Link>
                            <Nav.Link as={NavLink} to="/video" className={`nav-link-hover ${location.pathname === '/video' && 'active'}`}>Video</Nav.Link>
                            <Nav.Link as={NavLink} to="/faq" className={`nav-link-hover ${location.pathname === '/faq' && 'active'}`}>FAQ</Nav.Link>
                            <Nav.Link as={NavLink} to="/contactus" className={`nav-link-hover ${location.pathname === '/contactus' && 'active'}`}>Contact Us</Nav.Link>
                        </Nav>
                        <div className="d-flex align-items-center">
                            <div className='px-2'>
                                <a href="/searchMovie">
                                    <FontAwesomeIcon style={{ color: '#f0f0f0', paddingRight: "20px", width: "20", height: "20" }} icon={faSearch} title="Search Movie" />
                                </a>
                            </div>
                            <div className="d-flex align-items-center">
                                <div className="font-weight-bold">
                                    {username ? (
                                        <div>
                                            <img
                                                src="data:image/svg+xml,%3Csvg width='48' height='48' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Ccircle cx='24' cy='24' r='24' fill='%23FFFFFF'/%3E%3C/svg%3E"
                                                alt="SVG"
                                                style={{ width: '40px', height: '40px', borderRadius: '50%', marginRight: '10px', backgroundColor: 'green' }}
                                            />
                                            <b className='px-0'>{username}</b>

                                            <button className="btn btn-danger btn-sm btn-pill bg-transparent border-0 px-2" onClick={handleLogout} title="Log Out">
                                                <FontAwesomeIcon style={{ color: '#f0f0f0', paddingRight: "10px", width: "25", height: "25" }} icon={faSignOut} title="Log Out" />
                                            </button>
                                        </div>
                                    ) : (
                                        <div>
                                            <button className='btn btn-warning' style={{borderRadius: "10px"}} onClick={handleShowModal}>Login</button>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

        </div>
            
            <LoginModal isModalVisible={isModalVisible} handleCloseModal={() => setIsModalVisible(false)} />
        </>
    );
}

export default NavBar;
