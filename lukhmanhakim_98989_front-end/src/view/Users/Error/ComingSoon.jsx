import React from "react";
import { Container, Card } from 'react-bootstrap';
import NavigationBar from "../../NavBar/NavBar";
import Footer from "../../Footer/Footer";
import "../../../app/layout.css";

function ComingSoon() {
  return (
    <div className="main-container">
      <NavigationBar />
      <section>
        <div className="content-container">
          <Container className="content" style={{ minHeight: "calc(100vh - 128px)", display: "flex", flexDirection: "column", justifyContent: "space-between", marginTop: "200px", marginBottom: "10px" }}>
            <Card className="bg-secondary text-light" style={{ textAlign: "center", backgroundColor: "#f8f9fa", padding: "20px", borderRadius: "10px", marginBottom:"100px" }}>
              <h1 className="text-warning" >Coming Soon</h1>
            </Card>
          </Container>
        </div>
      </section>
      <Footer />
    </div>
  );
}

export default ComingSoon;


