
import React, { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import NavigationBar from '../../NavBar/NavBar'
import Footer from '../../Footer/Footer'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faClock } from '@fortawesome/free-solid-svg-icons';
import  axiosInstance from '../../../config/axiosConfig';
import "../../../app/layout.css";
import Form from 'react-bootstrap/Form';
import _ from 'lodash';

function SearchMovieDate() {

    const [movie, setMovie]                     = useState([]);
    const [loading, setLoading]                 = useState(true); 
    const [searchTerm, setSearchTerm]           = useState('');
    const [selectedDate, setSelectedDate]       = useState('');
    const [selectedDateEnd, setSelectedDateEnd] = useState('');
    

    function getCurrentDate() {

        const now   = new Date();
        const year  = now.getFullYear();
        const month = String(now.getMonth() + 1).padStart(2, '0'); 
        const day   = String(now.getDate()).padStart(2, '0');

        return `${year}-${month}-${day}`;
    }

    useEffect(() => {

        const fetchData = (async () => {
          try {

            const response =  await axiosInstance.get('api/newMovie/trending/search/date');
          
            if (typeof response.data === 'object' && response.data !== null) {
              setMovie(response.data);
              setLoading(false);
            } else {
              
              console.error('Response data is not an object:', response.data);
            }
          } catch (error) {

            console.error("An error occurred:", error);
          
        } finally {

            setLoading(false);
        }
        });

        fetchData();

    }, []);
    

    const handleSearch = (event) => {
        event.preventDefault();
        setLoading(true);
        if (!selectedDate || selectedDate === "") {
            console.log('Selected Date:', selectedDate || getCurrentDate());
        }
    
        const params = {
            date        : selectedDate || getCurrentDate(), 
            dateEnd     : selectedDateEnd || getCurrentDate(),
            theater_name: searchTerm 
        };
    
        const fetchData = _.debounce(async () => {
            try {

                const response = await axiosInstance.get('api/newMovie/trending/search/date', { params });

                if (typeof response.data === 'object' && response.data !== null) {
                  setMovie(response.data); 
                } else {
                  console.error('Response data is not an object:', response.data);
                }

            } catch (error) {
                console.log("An error occurred:", error);
            } finally {
                setLoading(false);
            }
        },500);

        fetchData();

        return()=>{
            fetchData.cancel();
        }
       
    };
    
    return (
        <div className="main-container">
            <style>
                {`
                .center {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    margin-top: 3%;
                    padding-top: 3%;
                    margin-bottom: 5%;
                    padding-bottom: 5%;
                }
        
                .center img {
                    max-width: 100%;
                    height: auto;
                }
        
                .text-right {
                    text-align: right;
                }
        
                .text-left {
                    text-align: left;
                }
        
                .img-fluid {
                    width: 20%;
                    height: 10px;
                }
        
                .hover-dark:hover {
                    filter: brightness(70%);
                }
        
                .title-link {
                    text-decoration: none;
                    color: white;
                    font-size: 24px;
                }

                @keyframes spin {
                    0% { transform: rotate(0deg); }
                    100% { transform: rotate(360deg); }
                }
        
                .spinning-image {
                    animation: spin 20s linear infinite;
                }
        
                @media only screen and (max-width: 768px) {
                    .spinning-image {
                        animation: none;
                    }
                }
        
                `}
            </style>
        
            <NavigationBar />
            <Container>
                <section width="1440px">
                    <div className="center">
                        <div className="d-flex justify-content-center">
                            {/* <img src={process.env.PUBLIC_URL + '/logoPlay.png'} alt="Logo" className="img-fluid spinning-image" style={{ width: '35%', height: '35%' }} /> */}
                            <div className="px-1 spinning-image">
                                <svg width="274" height="274" viewBox="0 0 274 274" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="137" cy="137" r="60.8889" fill="#FED530"/>
                                    <path d="M125.583 155.778V118.221L153.744 137L125.583 155.778ZM129.389 125.334V148.666L146.894 137L129.389 125.334Z" fill="black"/>
                                    <path d="M274 137C274 212.663 212.663 274 137 274C61.337 274 0 212.663 0 137C0 61.337 61.337 0 137 0C212.663 0 274 61.337 274 137ZM2.73999 137C2.73999 211.15 62.8502 271.26 137 271.26C211.15 271.26 271.26 211.15 271.26 137C271.26 62.8502 211.15 2.73999 137 2.73999C62.8502 2.73999 2.73999 62.8502 2.73999 137Z" fill="#808080"/>
                                    <path d="M271.26 137C272.773 137 274.003 138.227 273.973 139.74C273.472 164.783 266.113 189.23 252.673 210.408C238.743 232.359 218.855 249.892 195.332 260.961C171.809 272.03 145.622 276.178 119.829 272.92C94.037 269.661 69.7042 259.132 49.6729 242.56C29.6416 225.989 14.7389 204.06 6.70526 179.335C-1.32835 154.61 -2.16116 128.11 4.30411 102.929C10.7694 77.7489 24.2658 54.9277 43.217 37.1313C61.502 19.9606 84.1365 8.15159 108.642 2.96698C110.123 2.65376 111.559 3.63166 111.842 5.11811C112.126 6.60457 111.15 8.03658 109.669 8.35104C86.185 13.34 64.4952 24.6672 46.9684 41.126C28.7752 58.2106 15.8186 80.119 9.61194 104.292C3.40528 128.466 4.20478 153.906 11.917 177.642C19.6293 201.378 33.936 222.429 53.166 238.338C72.396 254.246 95.7555 264.355 120.516 267.483C145.277 270.611 170.416 266.629 192.998 256.003C215.581 245.376 234.673 228.544 248.046 207.472C260.929 187.171 267.991 163.743 268.491 139.74C268.523 138.227 269.747 137 271.26 137Z" fill="#FED530"/>
                                </svg>
                            </div>
                            <div>
                                <Container className="text-left mt-2 pt-2 pb-0 mb-0">
                                    <h1>Search your movies here!</h1>
                                    <form onSubmit={handleSearch}>
                                        <div className="form-group">
                                            <Row>
                                                <div className="col-12">
                                                    <div className="input-group mb-3 px-0">
                                                        <input type="text" className="form-control rounded-pill" id="searchInput" placeholder="Search by theatre" value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} autoFocus />
                                                    </div>
                                                </div>
                                            </Row>
                                            <Row>
                                                <div className="col-6">
                                                    <div className="input-group mb-3">
                                                        <label className='px-3 pt-1' htmlFor="start_date">Start Date</label>
                                                        {/* <input type="date" className="form-control rounded-pill" id="date_start" value={selectedDate || getCurrentDate()} onChange={(e) => setSelectedDate(e.target.value)} style={{ cursor: 'pointer' }} /> */}
                                                        <Form.Control 
                                                            type="datetime-local" 
                                                            className="rounded-pill" 
                                                            id="dateTimePicker" 
                                                            value={selectedDate} 
                                                            onChange={(e) => setSelectedDate(e.target.value)} 
                                                            style={{ cursor: 'pointer' }} 
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <div className="input-group mb-3">
                                                        <label className='px-3 pt-1 mt-1' htmlFor="end_date">End Date</label>
                                                        {/* <input type="date" className="form-control rounded-pill" id="date_end" value={selectedDateEnd || getCurrentDate()} onChange={(e) => setSelectedDateEnd(e.target.value)} style={{ cursor: 'pointer' }} /> */}
                                                        <Form.Control 
                                                            type="datetime-local" 
                                                            className="rounded-pill" 
                                                            id="date_end" value={selectedDateEnd || getCurrentDate()} onChange={(e) => setSelectedDateEnd(e.target.value)}
                                                            style={{ cursor: 'pointer' }} 
                                                        />
                                                    </div>
                                                </div>
                                            </Row>

                                            <Row>
                                                <div className="col-12 mt-1 pt-1 text-center"> 
                                                    <button type="submit" className="btn btn-warning rounded-pill mt-0 col-12 col-sm-12 me-sm-4"> 
                                                        <b>Search</b>
                                                    </button>
                                                </div>
                                            </Row>
                                        </div>
                                    </form>
                                </Container>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                     <Row className='container'>
                        {!loading && <h1>Search Results ({Object.keys(movie).length ?? '0'})</h1>}
                    </Row>
                                    
                    {!loading ? (
                    <Row className="row g-2 pt-0 mt-0 pb-0 mb-0">
                    <div className="d-flex flex-wrap">
                        {Object.values(movie).map(movie => (
                            <React.Fragment key={movie.Movie_ID}>
                                <Col xs={12} sm={6} md={4} lg={3} className='px-1'>
                                    <Card className="bg-dark text-white mb-3 hover-dark m-2" style={{ 
                                        backgroundImage: `url(${movie.Poster})`, 
                                        backgroundSize: 'cover',
                                        height: '560px',
                                        weight: '400px',
                                        border: 'none',
                                        display: 'flex',
                                        flexDirection: 'column'
                                    }}>
                                        <div className="card-body">
                                            <label className=' btn btn-warning btn-sm rounded-pill'>
                                                {movie.Genre ?? '-'}
                                            </label>
                                        </div>
                
                                        <div className="card-footer" style={{ marginTop: 'auto', opacity: '5.0', backgroundColor: 'rgba(0, 0, 0, 0.7)', padding: '10px', color: 'white'}}>
                                            <p> 
                                                <small className='px-0'>
                                                    <label htmlFor="" className=''>
                                                            <FontAwesomeIcon style={{ width: "16px", height: "16px", border: "none" }} icon={faClock} />
                                                    </label>
                                                    <label htmlFor="" className='px-1'> {movie.Duration ?? '-'}</label>
                                                </small>
                                                
                                                <small  className='px-4'>
                                                        <label htmlFor="" className=''>
                                                            <FontAwesomeIcon style={{ width: "16px", height: "16px", border: "none" }} icon={faEye} />
                                                        </label>
                                                    <label htmlFor="" className='px-1'> {movie.Views ?? '-'}</label>
                                                </small>
                                            </p>
                
                                            <a className='title-link' href='#'>
                                                <h1 style={{ color: 'white' }}>{movie.Title ?? '-'}</h1>
                                            </a>

                                            <Row className="mt-0 pt-0 mb-1">
                                                    <a className='title-link text-right align-middle text-underline' href='#'>
                                                        <small style={{ color: 'white' }}>
                                                        <button className='btn btn-success' onClick={() => {
                                                            const message = `Successfully Booked !! \n\nTicket Movie:\n${movie.Title}\nTheater:\n${movie.Theater_name}\n\nStart_time:\n${movie.Start_time}\nEnd_time:\n${movie.End_time}`;
                                                            alert(message);
                                                        }}>
                                                            Book Now
                                                        </button>                                               
                                                        </small>
                                                    </a>
                                                </Row>
                                        </div>
                                    </Card>
                                </Col>
                            </React.Fragment>
                        ))}
                    </div>
                </Row>
                    ): 
                    (
                        <div className="mt-2 pb-5 mb-5 container">
                            <div className="text-left">
                                {loading && <h1 style={{ textShadow: "0 0 50px rgba(255, 255, 255, 3.0)" }}>Loading...</h1>}
                            </div>
                        </div>
                    )}
                    {!loading && Object.keys(movie).length === 0 && (
                        <div className="mt-2 pb-5 mb-5 container">
                            <div className="text-left">
                                <h1 className='pb-0 mb-0'>No record is found....</h1>
                            </div>
                        </div>
                    )}
                </section>
            </Container>
            <br /><br />        
            <Footer/>
        </div>
    );
}

export default SearchMovieDate;