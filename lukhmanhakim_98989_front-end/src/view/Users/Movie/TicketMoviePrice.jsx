import React from "react";
import { Container, Row, Col, Button } from 'react-bootstrap';
import NavigationBar from "../../NavBar/NavBar";
import Footer from "../../Footer/Footer";
import Card from 'react-bootstrap/Card';
import "../../../app/layout.css";

function TicketMoviePrice() {
  
  const ticketTypes = [
    { type: "Children", price: "RM 10", choose: "Choose" },
    { type: "Adult", price: "RM 15", choose: "Choose" },
    { type: "Senior", price: "RM 5", choose: "Choose" },
    { type: "Student", price: "RM 20", choose: "Choose" }
  ];

  const ticketPlanMember = [
    { type: "Basic", ticketPlanMember: "RM 50", choose: "Choose" },
    { type: "Standard", ticketPlanMember: "RM 100", choose: "Choose" },
    { type: "Premium", ticketPlanMember: "RM 150", choose: "Choose" },
    { type: "Special", ticketPlanMember: "RM 200", choose: "Choose" },
  ];

  return (
    <div className="main-container" style={{ background: "#333", color: "white", minHeight: "100vh" }}>
      <NavigationBar />
      <section>
        <Container className="mb-4 mt-4 pt-4 bg-dark text-warning" style={{ borderRadius: "10px" }}>
          <h1 className="text-center mb-4 text-warning">Ticket Price & Plans</h1>
          <Row className="justify-content-center mb-4 mb-4 mt-4 pt-4 pb-5 ">
            <Col md={6} className="mt-3">
              <Card style={{ borderRadius: "20px", background: "#444", color: "white" }}>
                <div className="card-body" style={{ borderRadius: "20px",backgroundColor: "black" }}>
                  <h2 className="text-center mb-3 text-warning">Price Ranges</h2>
                  <div className="table-responsive">
                    <table className="table table-bordered">
                      <thead>
                        <tr>
                          <th>Type/Price</th>
                          {ticketTypes.map((ticket, index) => (
                            <th key={`ticketType-${index}`}>{ticket.type}</th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="font-weight-bold">Price</td>
                          {ticketTypes.map((ticket, index) => (
                            <td key={`ticketPrice-${index}`}>{ticket.price}</td>
                          ))}
                        </tr>
                        <tr>
                          <td className="font-weight-bold">Choose</td>
                          {ticketTypes.map((ticket, index) => (
                            <td key={`ticketChoose-${index}`}>
                              <Button variant="primary" size="sm">{ticket.choose}</Button>
                            </td>
                          ))}
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={6} className="mt-3">
              <Card style={{ borderRadius: "20px", background: "#444", color: "white" }}>
                <div className="card-body" style={{  borderRadius: "20px" , backgroundColor: "black" }}>
                  <h2 className="text-center mb-3 text-warning">Ticket Plans</h2>
                  <div className="table-responsive">
                    <table className="table table-bordered">
                      <thead>
                        <tr>
                          <th>Package</th>
                          {ticketPlanMember.map((member, index) => (
                            <th key={`ticketPlan-${index}`}>{member.type}</th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="font-weight-bold">Price</td>
                          {ticketPlanMember.map((member, index) => (
                            <td key={`ticketPlanPrice-${index}`}>{member.ticketPlanMember}</td>
                          ))}
                        </tr>
                        <tr>
                          <td className="font-weight-bold">Choose</td>
                          {ticketPlanMember.map((member, index) => (
                            <td key={`ticketPlanChoose-${index}`}>
                              <Button variant="primary" size="sm">{member.choose}</Button>
                            </td>
                          ))}
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
      <Footer />
    </div>
  );
}

export default TicketMoviePrice;

