import React, { createContext, useState, useEffect } from 'react';

// Create a new context for authentication
const AuthContext = createContext();

// Define the AuthProvider component to manage authentication state
const AuthProvider = ({ children }) => {
  // Initialize the username state with null
  const [username, setUsername] = useState(null);

  // useEffect hook to run once during component initialization
  useEffect(() => {
    // Retrieve stored user data from sessionStorage
    const storedUserData = sessionStorage.getItem('userData');

    // Check if storedUserData exists
    if (storedUserData) {
      // Parse the stored user data
      const userData = JSON.parse(storedUserData);
      // Extract username from parsed user data or set it to an empty string if not available
      const username = userData.username || '';
      // Set the username state with the extracted username
      setUsername(username);
    } else {
      // If no stored user data exists, set the username state to null
      setUsername(null);
    }
  }, []); // Empty dependency array ensures this effect runs only once during component initialization

  // Return the AuthContext.Provider with the username state and its setter function as the value
  return (
    <AuthContext.Provider value={{ username, setUsername }}>
      {children} {/* Render the children components wrapped by the AuthProvider */}
    </AuthContext.Provider>
  );
};

// Export the AuthProvider and AuthContext to be used elsewhere in the application
export { AuthProvider, AuthContext };
