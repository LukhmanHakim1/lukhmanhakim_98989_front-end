
import React, { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import Footer from '../Footer/Footer'
import NavigationBar from '../NavBar/NavBar'
import "../../../src/app/layout.css";
import  axiosInstance from "../../config/axiosConfig"

function Home() {
    
    const [listMovie, setMovie]            = useState([]);
    const [loading, setLoading]            = useState(true); 
    const [moviePerPage]                   = useState(3); 
    const [currentPage, setCurrentPage]    = useState(1);

    useEffect(() => {
        // axiosInstance.get('api/newMovie')
        axios.get('https://663dd111e1913c4767958526.mockapi.io/api/pcarimovie/movie')
            .then(response => {
                const movieData = response.data; 
                setMovie(movieData);
                setLoading(false); 
            })
            .catch(error => {
                console.log(error);
                setLoading(false);
            });
    }, []);

    return (
        <div className="main-container">
            <style>
                {`
                .nav-link-hover {
                    position: relative;
                    color: white;
                    filter: brightness(300%);
                }
               
                @media (max-width: 767px) {
                    .nav-link-hover {
                        color: rgba(255, 255, 255, 0.8);
                    }
                }
                
                .nav-link-hover:hover::after {
                    content: '';
                    position: absolute;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    height: 2px;
                    background-color: yellow;
                }

                .center {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    margin-top:3%;
                    padding-top:3%;
                    margin-bottom:5%;
                    padding-bootom:5%;
                }

                .center img {
                    max-width: 100%;
                    height: auto;
                }

                .text-right {
                    text-align: right;
                }

                .text-left {
                    text-align: left;
                }

                .img-fluid{
                    width: 20%;
                    height: 10px;
                }

                .hover-dark:hover {
                    filter: brightness(90%);
                }

                .title-link {
                    text-decoration: none;
                    color: white; 
                    font-size: 24px;
                }

                .footer {
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                    background-color: #343434;
                    color: white;
                    text-align: center;
                    padding: 20px 0;
                }

                @keyframes spin {
                    0% { transform: rotate(0deg); }
                    100% { transform: rotate(360deg); }
                  }
                  
                  .spinning-image {
                    animation: spin 20s linear infinite;
                  }

                  @media only screen and (max-width: 768px) {
                    .spinning-image {
                      animation: none;
                    }
                  }

                `}
            </style>
        
        <NavigationBar/>
        <Container>

        <section className='mb-0 pb-0'>
               <div className="center">
                    <div className="d-flex justify-content-center">
                            {/* <img src={process.env.PUBLIC_URL + '/logoPlay.png'} alt="Logo" className="img-fluid spinning-image" style={{ width: '274px', height: '274px', top:'257px' }} /> */}
                            <div  className="px-1 spinning-image">
                                <svg width="274" height="274" viewBox="0 0 274 274" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="137" cy="137" r="60.8889" fill="#FED530"/>
                                    <path d="M125.583 155.778V118.221L153.744 137L125.583 155.778ZM129.389 125.334V148.666L146.894 137L129.389 125.334Z" fill="black"/>
                                    <path d="M274 137C274 212.663 212.663 274 137 274C61.337 274 0 212.663 0 137C0 61.337 61.337 0 137 0C212.663 0 274 61.337 274 137ZM2.73999 137C2.73999 211.15 62.8502 271.26 137 271.26C211.15 271.26 271.26 211.15 271.26 137C271.26 62.8502 211.15 2.73999 137 2.73999C62.8502 2.73999 2.73999 62.8502 2.73999 137Z" fill="#808080"/>
                                    <path d="M271.26 137C272.773 137 274.003 138.227 273.973 139.74C273.472 164.783 266.113 189.23 252.673 210.408C238.743 232.359 218.855 249.892 195.332 260.961C171.809 272.03 145.622 276.178 119.829 272.92C94.037 269.661 69.7042 259.132 49.6729 242.56C29.6416 225.989 14.7389 204.06 6.70526 179.335C-1.32835 154.61 -2.16116 128.11 4.30411 102.929C10.7694 77.7489 24.2658 54.9277 43.217 37.1313C61.502 19.9606 84.1365 8.15159 108.642 2.96698C110.123 2.65376 111.559 3.63166 111.842 5.11811C112.126 6.60457 111.15 8.03658 109.669 8.35104C86.185 13.34 64.4952 24.6672 46.9684 41.126C28.7752 58.2106 15.8186 80.119 9.61194 104.292C3.40528 128.466 4.20478 153.906 11.917 177.642C19.6293 201.378 33.936 222.429 53.166 238.338C72.396 254.246 95.7555 264.355 120.516 267.483C145.277 270.611 170.416 266.629 192.998 256.003C215.581 245.376 234.673 228.544 248.046 207.472C260.929 187.171 267.991 163.743 268.491 139.74C268.523 138.227 269.747 137 271.26 137Z" fill="#FED530"/>
                                </svg>
                            </div>
                            
                        <div className="container text-left mt-5 pt-5 pb-0 mb-0">
                            <h1>
                                Find your movies here!
                            </h1>
                            <small>Explore our gallery full of exciting films from all around the globe.</small>
                                <br />
                            <small>Enjoy your entertainment without hidden charges or disturbing ads.</small>
                        </div>
                    </div>
                </div>
        </section>

        <section className='mb-5 pb-5'>
               {/* array map data in array */}
                <Row className="text-left">
                    {listMovie.map((movie, index) => (
                        <Col key={index} xs={12} md={index === 0 ? "8" : "4"}>
                            <div className="card bg-dark text-white mb-3 mx-0 px-0 hover-dark" style={{ 
                                backgroundImage: `url(${movie.Poster ? movie.Poster : '-'})`,                                
                                backgroundSize: 'cover',
                                height: index === 0 ? '350px' : index === 1 ? '350px' :'250px',
                                border: 'none',
                                display: 'flex',
                                flexDirection: 'column'
                            }}>
                                <div className="card-body">
                                    <label className=' btn btn-warning btn-sm rounded-pill'>{movie.Genre ?? '-'}</label>
                                </div>

                                <div className="card-footer" style={{ marginTop: 'auto' ,  opacity: '5.0', backgroundColor: 'rgba(0, 0, 0, 0.7)' }}>
                                    <p> 
                                        <small className='px-0 align-middle'>
                                            <label htmlFor="" className=''>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-clock" viewBox="0 0 16 16">
                                                    <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71z"/>
                                                    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16m7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0"/>
                                                </svg>
                                            </label>
                                            <label htmlFor="" className='px-1 align-middle'> {movie.Duration ?? '-'}</label>
                                        </small>
                                    
                                        <small  className='px-4 align-middle'>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-eye" viewBox="0 0 16 16">
                                                <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8M1.173 8a13 13 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5s3.879 1.168 5.168 2.457A13 13 0 0 1 14.828 8q-.086.13-.195.288c-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5s-3.879-1.168-5.168-2.457A13 13 0 0 1 1.172 8z"/>
                                                <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5M4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0"/>
                                            </svg>
                                            <label htmlFor="" className='px-1'> {movie.Views ?? '-'}</label>
                                        </small>
                                    </p>
                                    <a className='title-link align-middle' href='#'><h1 style={{ color: 'white' }}>{movie.Title ?? '-'}</h1></a>
                                    <div className="row mt-0 pt-0 mb-1">
                                        <a className='title-link text-right align-middle text-underline' href='#'  
                                                onClick={() => alert(
                                                    `Title:\n${movie.Title}\n\n` +
                                                    `Description:\n${movie.Description}\n\n` +
                                                    `Rating:\n${movie.Overall_rating}\n\n`
                                                )}
                                        ><small style={{ color: 'white' }}>Read More..</small></a>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    ))}
                </Row>
        </section>
    </Container>    
    <Footer/>
    </div>
    );
}

export default Home;
