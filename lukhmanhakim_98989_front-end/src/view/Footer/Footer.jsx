import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faMapMarker, faPhone, faArrowRight } from '@fortawesome/free-solid-svg-icons';
import React from 'react';

function Footer() {
    return (
<footer style={{
    left: 0,
    bottom: 0,
    width: 'auto',
    paddingLeft:'auto',
    paddingright:'auto',
    color: 'white',
    textAlign: 'center',
    zIndex: 1000,
}}>
    <div className=" container-fluid mt-0 pt-0">
        <div className=" container-fluid text-center  mb-0 pb-0">
            <div className="row">
                <div className="col-lg-4 col-md-12" style={{ backgroundColor: '#3D3D3D' }} >
                    <div className="row text-left px-3 mt-5 pt-5">
                        <div className='col'>
                            <div className="pb-3">
                                <h1>PcariMovie</h1>
                                <small>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et.
                                </small>
                            </div>
                            <div className="pb-4 pt-4">
                                <small>
                                    Join Newsletters
                                </small>
                            </div>
                            <form className="mb-5 pb-5" action="#">
                                <div className="input-group">
                                <input 
                                    type="email" 
                                    className="form-control bg-secondary" 
                                    style={{ border: "none", color: "white", "::placeholder": { color: "white" } }} 
                                    placeholder="Insert your email here" 
                                />
                                <div className="input-group-append">
                                <button className="btn btn-secondary" type="submit">
                                        <div className='bg-warning btn btn-sm btn-pill'>
                                            <span className=" bg-warning btn btn-sm btn-pill">
                                            <FontAwesomeIcon style={{ width: "25px", height: "25px", color: "black", border: "none" }} icon={faArrowRight} />
                                            </span>
                                        </div>
                                    </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col-lg-8 col-md-12" style={{ backgroundColor: '#525252' }}>
                    <div className="row justify-content-left px-5 align-middle mt-5 pt-">
                        <div className="col-lg-12 text-left  col-md-12 px-5 mt-0 pt-5" style={{ backgroundColor: '#525252' }}>
                        <div className="row pb-5 px-0">
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Product</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Movies</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Tv Show</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Videos</a></div>
                        </div>
                        <div className="row pb-5 px-0">
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Media Group</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Nice Studio</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Nice News</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Nice TV</a></div>
                        </div>
                        <div className="row pb-4 px-0">
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Site Map</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>About</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Career</a></div>
                            <div className="col px-4"><a href="#" style={{ color: 'white', textDecoration: 'none', fontWeight: 'bold' }}>Press</a></div>
                        </div>

                        </div>
                        <div className="row pb-3 px-0 pt-2">
                            <div className="row pb-3 px-0 pt-2 align-middle">
                                <div className="col-md-6 col-sm-12" style={{ transform: 'translate(0 px)' }}>
                                        <FontAwesomeIcon style={{ width:"16",height:"16" }}  icon={faMapMarker} />
                                        <small style={{ marginLeft: '5px' }}>8819 Ohio St. South gate, California 90280</small>
                                    </div>
                                    <div className="col-md-3 col-sm-12" style={{ transform: 'translate(-60px)' }}>
                                        <FontAwesomeIcon style={{ width:"16",height:"16" }}  icon={faEnvelope} />
                                        <small style={{ marginLeft: '5px' }}>ourstudio@hello.com</small>
                                    </div>
                                    <div className="col-md-3 col-sm-12" style={{ transform: 'translate(-70px)' }}>
                                        <FontAwesomeIcon style={{ width:"16",height:"16" }}  icon={faPhone} />
                                        <small style={{ marginLeft: '4px' }}>+271 386-647-3639</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</footer>

    )
}


export default Footer;



