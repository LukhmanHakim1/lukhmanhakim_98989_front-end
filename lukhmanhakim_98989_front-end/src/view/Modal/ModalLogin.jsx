import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import axiosInstance from '../../config/axiosConfig'
import _ from 'lodash';


function LoginModal({ isModalVisible, handleCloseModal }) {

    const [username, setUsername]           = useState('');
    const [password, setPassword]           = useState('');
    const [usernameError, setUsernameError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const CryptoJS                          = require('crypto-js');
    const secretKey                         = 'abc';
    
    const handleLogin = _.debounce(() => {

        if (!username) {
            setUsernameError(true);
        } else {
            setUsernameError(false);
        }

        if (!password) {
            setPasswordError(true);
        } else {
            setPasswordError(false);
        }

        if (username && password) {

            const userData = {
                username: username,
                password:  CryptoJS.AES.encrypt(password,secretKey).toString()
            };

            // axios.get('https://663dd111e1913c4767958526.mockapi.io/api/pcarimovie/users')
            axiosInstance.get('/api/logIn')
            .then(response => {
               
                const login          = response.data;

                console.log(login);

                const matchedUser    = login.find(

                                        login => login.username === userData.username,
                                        login => login.password === userData.password
                                       
                                    );

                if(matchedUser){

                    const jsonString = JSON.stringify(matchedUser);

                    sessionStorage.setItem('userData', jsonString);

                    handleCloseModal();
                    alert('Successfully logged in !!');
                    window.location.reload();
                    
                }else{
                    
                    handleCloseModal();
                    alert('Please register before log in !!');
                }

            })
            .catch(error => {
                console.log(error);
                
            });
   
        }
    },300);

    return (
        <Modal show={isModalVisible} onHide={handleCloseModal} centered>
            <div className='bg-dark text-light' style={{ borderRadius: '5px' }}>
                <div className=''>
                <Modal.Header className='bg-warning align-middle' closeButton>
                    <Row>
                        <Col>
                            <Modal.Title className='align-middle'><h4>Login</h4></Modal.Title>
                        </Col>
                    </Row>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row className="mb-2 pb-2">
                            <Col className="md-12">
                                <div className="border border-light p-2">
                                    <small>Note<font className="text-danger px-1">*</font></small>
                                    <br />
                                    <small>
                                        Username: John Glich
                                    </small>
                                    <br />
                                    <small>
                                        Password: abcd1234
                                    </small>
                                </div>
                            </Col>
                        </Row>

                        <form>
                            <div className="input-group mb-1 pb-1">
                                <div className="input-group-prepend px-1" style={{ height: '38px', display: 'flex', alignItems: 'center' }}>
                                    <span className="input-group-text" id="basic-addon1" style={{ height: '100%' }}>
                                        <FontAwesomeIcon icon={faUser} style={{ height: '100%' }} />
                                    </span>
                                </div>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    placeholder="Username" 
                                    aria-label="Username" 
                                    aria-describedby="basic-addon1" 
                                    style={{ height: '38px', borderRadius: '0 10px 10px 0' }} 
                                    value={username} 
                                    onChange={(e) => setUsername(e.target.value)} 
                                />
                            </div>
                           <div className="mb-2">
                                {usernameError && <small className="text-danger">Please enter username</small>}
                           </div>

                            <div className="input-group mb-1 pb-1">
                                <div className="input-group-prepend px-1" style={{ height: '38px', display: 'flex', alignItems: 'center' }}>
                                    <span className="input-group-text" id="basic-addon2" style={{ height: '100%' }}>
                                        <FontAwesomeIcon icon={faLock} style={{ height: '100%' }} />
                                    </span>
                                </div>
                                <input 
                                    type="password" 
                                    className="form-control" 
                                    placeholder="Password" 
                                    aria-label="Password" 
                                    aria-describedby="basic-addon2" 
                                    style={{ height: '38px', borderRadius: '0 10px 10px 0' }} 
                                    value={password} 
                                    onChange={(e) => setPassword(e.target.value)} 
                                />
                            </div>
                            <div className="mb-2">
                                {passwordError && <small className="text-danger">Please enter password</small>}
                            </div>
                        </form>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Row>
                        <Col className='md-12' style={{borderRadius: "10px"}}>
                            <button className="btn btn-primary"onClick={handleLogin}>Login</button>
                        </Col>
                    </Row>
                </Modal.Footer>
                </div>
            </div>
        </Modal>
    );
}

export default LoginModal;
