import axios from 'axios';


// By default we call it like this //

// const response = await axios.get('http://127.0.0.1:3005/api/searchMovieDate', { params })


const instance = axios.create({
  baseURL: 'http://127.0.0.1:3005'
});


export default instance;
