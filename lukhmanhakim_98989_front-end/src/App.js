import React, { useContext } from 'react';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom'; // Importing necessary components from react-router-dom
import { AuthProvider, AuthContext } from './view/Authentication/auth'; // Importing AuthProvider and AuthContext from authentication module
import Home from './view/Home/Home'; // Importing Home component
import SearchMovie from './view/Users/Movie/SearchMovie'; // Importing SearchMovie component
import 'bootstrap/dist/css/bootstrap.min.css'; // Importing Bootstrap CSS
import SearchMovieDate from './view/Users/Movie/SearchMovieDate'; // Importing SearchMovieDate component
import Pricing from './view/Users/Movie/TicketMoviePrice'; // Importing Pricing component
import ComingSoon from './view/Users/Error/ComingSoon'; // Importing ComingSoon component


function App() {
  return (
    <AuthProvider> {/* Wrapping the entire application with AuthProvider for authentication */}
    {/* Providing the BrowserRouter for client-side routing */}
      <BrowserRouter> 
      {/* Defining routes for different paths */}
        <Routes> 
          {/* Unprotected route */}
          <Route path="/home" element={<Home />} /> 
          {/* Unprotected route */}
           <Route path="/searchMovie" element={<SearchMovie />} /> 
          {/* Protected route requiring authentication */}
          <Route path="/searchMovieDate" element={<ProtectedRoute component={SearchMovieDate} />} /> 
          {/* Unprotected route */}
          <Route path="/ticketPrice" element={<Pricing />} /> 
          {/* Unprotected route */}
          <Route path="/movie" element={<ComingSoon />} /> 
          {/* Unprotected route */}
          <Route path="/tvshow" element={<ComingSoon />} /> 
          {/* Unprotected route */}
          <Route path="/video" element={<ComingSoon />} /> 
          {/* Unprotected route */}
          <Route path="/faq" element={<ComingSoon />} /> 
          {/* Unprotected route */}
          <Route path="/contactus" element={<ComingSoon />} /> 
          {/* Default route, also unprotected */}
          <Route path="/" element={<Home />} /> 
          {/* If the route not exists */}
          <Route path="*" element={<Navigate to="/home" />} />
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  );
}

//Function to protect the route 
function ProtectedRoute({ component: Component, ...rest }) {
  
  // Getting username from authentication context
  const { username } = useContext(AuthContext); 

  // If user is authenticated  , render the requested component, else navigate to '/home'
  return username ? <Component {...rest} /> : <Navigate to="/home" replace />;
}

export default App;
