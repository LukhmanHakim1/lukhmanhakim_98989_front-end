# PcariMovie
PCariMovie is a comprehensive platform designed to simplify the process of booking movie theater tickets. Combining a user-friendly frontend built with React JS and a robust backend powered by Laravel, PCariMovie offers a seamless experience for movie enthusiasts.  

# Front-End
React Js :

Installation 
1. To run it need to open the terminal and run command "npm install" to install or update the library and dependency.
2. Type and run command "npm start" to run the page for front-end. 
3. Lastly it should be run on port "3000". 


# User-Interface

# HOME PAGE
![Image 1](https://i.postimg.cc/RC8mjB9Y/Capture1.png)

# SEARCH MOVIE PAGE
![Image 2](https://i.postimg.cc/Sxbhxfn7/Capture2.png)

# SEARCH MOVIE BY RANGE DATE PAGE
![Image 3](https://i.postimg.cc/y8hzg39q/Capture3.png)

# TICKET PRICE PAGE
![Image 4](https://i.postimg.cc/FsLvvBHH/Capture4.png)

# LOGIN PAGE
![Image 5](https://i.postimg.cc/hGSR94bX/Capture5.png)








